console.log("Hello world!");

function addTwoNumber(num1, num2){
    console.log("The sum of " +num1 +" and " +num2 +" is:")
    console.log(num1 + num2);
}

function subtractTwoNumber(num1, num2){
    console.log("The difference of " +num1 +" and " +num2 +" is:")
    console.log(num1 - num2);
}

addTwoNumber(5, 15);
subtractTwoNumber(20, 5);

function multiplyTwoNumber(num1, num2){
    console.log("The product of " +num1 +" and " +num2 +" is:")
    return num1 * num2;
}

function divideTwoNumber(num1, num2){
    console.log("The quotient of " +num1 +" and " +num2 +" is:")
    return num1 / num2;
}

let product = multiplyTwoNumber(50,10);
console.log(product);

let quotient = divideTwoNumber(50,10);
console.log(quotient);


function getAreaOfCircle(radius){
    console.log("The result of getting the area of a circle with " +radius +" radius:");
    return Math.PI * Math.pow(radius, 2);
}
console.log(getAreaOfCircle(15));

function averageOfFourNumbers(num1, num2, num3, num4){
    console.log("The average of " +num1 +"," +num2 +"," +num3 +" and " +num4)
    return (num1 + num2 + num3 + num4) / 4;
}

let averageVar = averageOfFourNumbers(20, 40, 60, 80);
console.log(averageVar);

function getPercentage(score, total_score){
    let percentage = (score/total_score) * 100;
    console.log("Is " +score +"/" +total_score +" a passing score?")
    return percentage >= 75;
}

let isPassed = getPercentage(38,50);
console.log(isPassed);

