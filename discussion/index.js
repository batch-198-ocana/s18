// function with parameter
function printName(name){
    console.log("My name is " +name);
}

printName("Juan");

/*
    function fName(paramenter){
        statement;
    }
    
    fName(argument);
*/

function printMyAge(age){
    console.log("I am " +age);
}
printMyAge(25);
printMyAge(); // result: undefined

function checkDivisibilityby8(num){
    let remainder = num % 8;
    console.log("The remainder of " +num +" divided by 8 is: " +remainder);

    let isDivisibilityby8 = remainder === 0;
    console.log("Is " +num +" divisible by 8?");
    console.log(isDivisibilityby8);
}
checkDivisibilityby8(64);
checkDivisibilityby8(27);

// mini activity
function printMyFavoriteSuperhero(hero){
    console.log("My favorite superhero is " +hero);
}
printMyFavoriteSuperhero("Spiderman")

function printMyFavoriteLanguage(language){
    console.log("My favorite language is " +language);
}
printMyFavoriteLanguage("JavaScript");
printMyFavoriteLanguage("Java");
// end of mini activity

//function with multiple params
function printFullName(firstName, middleName, lastName){
    console.log(firstName +" " +middleName +" " +lastName);
}
printFullName("test","test","test");

    // var as args
    let firstName = "Chaz";
    let middleName = "Ara";
    let lastName = "Añaco";

    printFullName(firstName, middleName, lastName);

// mini activity
function printFavoriteSongs(param1, param2, param3, param4, param5){
    console.log("This are my favorite songs:");
    console.log(param1);
    console.log(param2);
    console.log(param3);
    console.log(param4);
    console.log(param5);
}
printFavoriteSongs("Saturn","Cleopatra","Amnesia","This is Gospel","Lost Boy");
// end of mini activity

// return statement
let fullName = printFullName("Someone","Anyone","Everyone");
console.log(fullName);

function returnFullName(firstName, middleName, lastName){
    return firstName +" " +middleName +" " +lastName;
}

fullName = returnFullName("Someone","Anyone","Everyone"); 
console.log(fullName +" is awesome.");

//mini activity
function createPlayerInfo(username, level, job){
    return "username: " +username + ", level: " +level +", job: " +job;
}

let user1 = createPlayerInfo("white_night",95,"Paladin");
console.log(user1);
//end of mini activity